// this.getMovim-js 

// Este mix in va a contener un solo método. tempg
//Recibe un arreglo y lo regresa con los calculos de la grid de ventas f4. 

var numeral = require('numeral');

import { mapGetters, mapState} from 'vuex'

export default {

    data () {
      return {
        tempg:[],
        importedoc:0.00,
        descuentodoc:0.00,
        total: 0.00,
        iva:0.00,
        subtotal:0.00,

     }   
   },  

   computed: {
      ...mapGetters('carrito',['getCarrito']),
      ...mapState('carrito',['carrito']),
    },

	methods:{

		formarCarrito(){
    		//1. Vamos a manejar igual que en Sait Tempg para grid de la venta.
        // Se manda llamar desde el Método al mixins.

        //2. Unformar para poder usar los numericos.
        for (var i = this.getCarrito.length - 1; i >= 0; i--) {
          this.getCarrito[i].importe = parseFloat(this.getCarrito[i].precio1)
          this.getCarrito[i].precio1 = parseFloat(this.getCarrito[i].precio1)
        }

        // OBJETO DOCUM 
        this.total = 0
        this.iva = 0
        this.subtotal = 0
        this.descuentodoc = 0
        this.importedoc = 0
        // console.log("Carrito", this.getCarrito)

        //hacer el subtotal, iva y total
        for (var i = this.getCarrito.length - 1; i >= 0; i--) {

          // Importe = Movim.CANT*Movim.PRECIO1
          var importepar = this.getCarrito[i].cantidad * this.getCarrito[i].precio1
          // console.log("Importe ", importepar)

          //descuento = Sum(Movim.CANT*Movim.PRECIO1 *Movim.PJEDESC *.01 
          var descuento = this.getCarrito[i].cantidad * this.getCarrito[i].precio1 * this.getCarrito[i].pjedesc * 0.01
          // console.log("Descuento ", descuento)

          //SubTotal  = Sum(Movim.CANT*Movim.PRECIO1*(1-Movim.PJEDESC *.01)
          var subtotalpar=this.getCarrito[i].cantidad *this.getCarrito[i].precio1 *(1-this.getCarrito[i].pjedesc*0.01)
          // console.log("subtotalpar", subtotalpar)

          //IVA = Sum(Movim.CANT*Movim.PRECIO1*(Movim.IMPUESTO1 *.01)*(1-Movim.PJEDESC*.01)) As IVA,
          // var ivapar = this.getCarrito[i].cantidad * this.getCarrito[i].precio1 * (this.getCarrito[i].impuesto1*0.01) *(1-this.getCarrito[i].pjedesc*0.01)
          // console.log("iva par", ivapar)

          // Total = Sum(Movim.CANT*Movim.PRECIO1*(1+Movim.IMPUESTO1 *.01)*(1-Movim.PJEDESC*.01) 
          var totalpar = this.getCarrito[i].cantidad * this.getCarrito[i].precio1 * (1-this.getCarrito[i].pjedesc*0.01)  
          // console.log("totalpar",totalpar)        

          //Actualizar vuex
          this.getCarrito[i].importe   = importepar
          this.getCarrito[i].descuento = descuento
          this.getCarrito[i].subtotal  = subtotalpar
          // this.getCarrito[i].iva       = ivapar
          this.getCarrito[i].total     = totalpar

          // Calcular Totales 
          this.importedoc   = this.importedoc   + importepar
          this.descuentodoc = this.descuentodoc + descuento
          this.subtotal     = this.subtotal     + subtotalpar
          // this.iva          = this.iva          + ivapar
          this.total        = this.total        + totalpar


          //Dar Formato.
          this.getCarrito[i].total = numeral(this.getCarrito[i].total).format('0,0.00')
          this.getCarrito[i].total = numeral(this.getCarrito[i].total).format('0,0.00')
          // this.getCarrito[i].importe = numeral((this.getCarrito[i].precio1 * this.getCarrito[i].cantidad)).format('0,0.00')
          this.getCarrito[i].importe = numeral(this.getCarrito[i].importe).format('0,0.00')
          this.getCarrito[i].precio1 = numeral(this.getCarrito[i].precio1).format('0,0.00')
          
        }

        //DAR FORMATO       
        this.total       = numeral(this.total).format('0,0.00')
        this.iva         = numeral(this.iva).format('0,0.00')
        this.subtotal    = numeral(this.subtotal).format('0,0.00')
        this.descuentodoc= numeral(this.descuentodoc).format('0,0.00')
        this.importedoc  = numeral(this.importedoc).format('0,0.00')

    }
	}	
}