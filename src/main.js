import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueResource from 'vue-resource'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'babel-polyfill'

Vue.config.productionTip = false
Vue.use(VueResource, VueAxios, axios)

// Vue.http.options.root = 'http://surtimedix.com:3001/'
Vue.http.options.root = 'http://localhost:3001/'
// http://www.testsnode.com/

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Accept', 'application/json')
  request.headers.set('Content-Type', 'application/json')
  next()
});


new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
