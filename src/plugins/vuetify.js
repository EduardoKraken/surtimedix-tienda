import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors'
import es from 'vuetify/es5/locale/es';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify);

export default new Vuetify({
	icons: {
    iconfont: 'md',
  },
  theme: {
		themes: {
			light: {
				primary: colors.blue.darken1, // AZUL
				secondary: '#004D40',// VERDE
				success: '#bf1c7f',    // ROSA
				error: '#E53935',   // NEGRO
				info: '#894975',  // MORADO
				warning: '#6f7170',     // GRIS
			},
		},
	},

  lang:{
    locales:{ es },
    current: 'es'
  }
});
