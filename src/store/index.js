import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'


import articulos from '@/modules/articulos'
import carrito from '@/modules/carrito/carrito'	

import login     from '@/modules/login/login'
import registro     from '@/modules/login/registro'




Vue.use(Vuex, VueAxios, axios)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
		articulos,
		carrito,
    login,
    registro

  }
})
