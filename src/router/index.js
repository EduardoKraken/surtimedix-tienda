import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home2.vue'
import store from '@/store'

// home
import Novedades from '@/views/home/Novedades.vue'
import Destacados from '@/views/home/Destacados.vue'

// Control de acceso
import Login            from '@/views/acceso/Login.vue'
import Registrarse      from '@/views/acceso/Registrarse.vue'
import RecuperarContra  from '@/views/acceso/RecuperarContra.vue'


import CatArts from '@/views/articulos/CatArts.vue'
import DetArt from '@/views/articulos/DetArt.vue'
import LabArt from '@/views/articulos/LabArt.vue'


import Carrito    from '@/views/Carrito.vue'


// Ajustes
import TabAjustes from '@/views/ajustes/TabAjustes.vue'
import Miperfil from '@/views/ajustes/MiPerfil.vue'
import Direcciones from '@/views/ajustes/Direcciones'
import DatosFiscales from '@/views/ajustes/DatosFiscales'
import Miscompras from '@/views/ajustes/MisCompras'
import VerPedido from '@/views/ajustes/VerPedido.vue'


Vue.use(Router)


var router = new Router({    
  mode: '',
  base: process.env.BASE_URL,
  routes: [


    //MODULO DE LOGIN
    { path: '/', name: 'home' , component: Home, 
      meta: { LIBRE: true}},
    { path: '/novedades', name: 'novedades' , component: Novedades, 
      meta: { LIBRE: true}},
    { path: '/destacados', name: 'destacados' , component: Destacados, 
      meta: { LIBRE: true}},


    { path: '/login', name: 'login' , component: Login, 
      meta: { LIBRE: true}},
    { path: '/registrarse', name: 'registrarse' , component: Registrarse, 
      meta: { LIBRE: true}},
    { path: '/recuperarcontra', name: 'recuperarcontra' , component: RecuperarContra, 
      meta: { LIBRE: true}},

    // Pedidos
    { path: '/carrito', name: 'carrito' , component: Carrito, 
      meta: { LIBRE: true}},
    { path: '/verpedido', name: 'verpedido', component: VerPedido,
      meta: { ADMIN: true }},

   
    // Articulos
    { path: '/catarts', name: 'catarts', component: CatArts,
      meta: { LIBRE: true }},
    { path: '/labart/:id', name: 'labart', component: LabArt,
      meta: { LIBRE: true }},
    { path: '/detart/:codigo', name: 'detart', component: DetArt,
      meta: { LIBRE: true }},

    
    // Ajustes
    { path: '/tabajustes', name: 'tabajustes', component: TabAjustes,
      meta: { ADMIN: true }},
    { path: '/miperfil', name: 'miperfil', component: Miperfil,
      meta: { ADMIN: true }},
    { path: '/datosfiscales', name: 'datosfiscales', component: DatosFiscales,
      meta: { ADMIN: true }},
    { path: '/direcciones', name: 'direcciones', component: Direcciones,
      meta: { ADMIN: true }},
    { path: '/miscompras', name: 'miscompras', component: Miscompras,
      meta: { ADMIN: true }},



  ]
})


router.beforeEach( (to, from, next) => {
  console.log(store)
  console.log(store.getters['login/getLogeado'])
  if(to.matched.some(record => record.meta.LIBRE)){
    next()
  }else if(store.getters['login/getLogeado'] == true){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else{
    next({
      name: 'login'
    })
  }
})

export default router
