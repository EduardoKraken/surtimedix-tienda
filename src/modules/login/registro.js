import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		registro:''
	},

	mutations:{

		ACTUALIZREGISTRO(state, registro){
			state.registro = registro
		}

	},

	actions:{

		// BUSCAR USUARIO
		buscarUsuarioWeb({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
				Vue.http.post('user.email', usuario) .then(respuesta=>{
					return respuesta.json()
				}).then(respuestaJson=>{
						// VALIDO LA RESPUESTA
					if(respuestaJson.length ==  0){
						// SI NO ENCUENTRA AL USUARIO MANDA A REGISTRARLO
						resolve(true)

					}else{
						// SI ENCUENTRO AL USUARIO REGRESO FALSO
						resolve(false)
					}

			  }).catch(error=>{console.log('error',error)})
			})
		},

		registrarUsuario({dispatch,commit}, usuario){
			return new Promise((resolve, reject) => {
				
				const payload = {
					nombre:       usuario.nombre,
					email:        usuario.email,
					password:     usuario.password
				}

				Vue.http.post('user.add',  payload) 
				.then(respuesta=>{ 
					console.log('user.add: ',respuesta)
					var data = {
						nombre:       usuario.nombre,
						email:        usuario.email,
						id:           respuesta.body.id
					}
					dispatch('enviarCorreo',data)
					resolve(true)
			  }).catch(error=>{
			  	console.log('error',error)
			  	resolve(false)
			  })
			})
		},

		enviarCorreo({},datos){
			// var url = 'http://surtimedix.com/#/activarusuario/'
			var url = 'http://localhost:8080/#/activarusuario/'
			
			var payload = {
				nombre:       datos.nombre,
				correo:       datos.email,
				url:          url + datos.id
			}


			Vue.http.post('correos.send',  payload).then(respuesta=>{ 
				console.log(respuesta)
		  }).catch(error=>{
		  	console.log('error',error)
		  	resolve(false)
		  })

		}
	},

	getters:{
		
		traerDatosUsuarios(state){
			return state.registro
		}

	}
}