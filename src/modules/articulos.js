import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		articulos:[],
		laboratorio:{}
	},

	mutations:{
		ARTICULOS(state, value){
			state.articulos = value
		},

		LABORATORIO(state, value){
			state.laboratorio = value
		},

	},

	actions:{
		//Función para cargar los avisos de
		traerArticulos({commit, dispatch}, usuario){
			//retornar una promesa (resolve, rejecet)
			return new Promise((resolve, reject) => {
			  Vue.http.get('admin/articulos.activos').then(respuesta=>{
		      commit('ARTICULOS',respuesta.body)
		     	resolve(true) 
			  }).catch(error=>{console.log(error)})
      })
		},

		routeLaboratorio({commit, dispatch}, lab){
			//retornar una promesa (resolve, rejecet)
      commit('LABORATORIO',lab)
		},

	 
		
  },

	getters:{
		getArticulos(state){
		  return state.articulos
		},

		getLab(state){
		  return state.laboratorio
		},

		
	}
}