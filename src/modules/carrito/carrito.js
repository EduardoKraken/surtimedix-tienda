import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
var accounting = require("accounting");

export default{
	namespaced: true,
	state:{
		carrito:[],
		carritovisible: false,
		tc: ''
	},

	mutations:{
		TC(state, value){
			state.tc = value
		},

		ADDCARRITO(state, value){
			//verificamos que el arreglo tenga más de un objeto
			if(state.carrito.length > 0){
				//hacemos un recorrido por el areglo de obetos
				for (var i = state.carrito.length - 1; i >= 0; i--) {
					//comparamos si la clave del nuevo articulo ya existe en el arreglo de objetos
					if(state.carrito[i].codigo === value.codigo){
						//si existe, incrementamos la cantidad en el nuevto 
						value.cantidad = state.carrito[i].cantidad + 1
						//eliminamos el objetos que tiene esa clavee
						state.carrito.splice(i, 1);
					}
				}
			}
			//añadimos el nuevo articulo
			state.carrito.push(value)
			console.log(state.carrito)
		},


		LIMPIAR(state, value){
			state.carrito = []
		},

		ACTUALIZARCARRITO(state, value){
			console.log("actualizar carrrito",state.carrito)
			for (var i = state.carrito.length - 1; i >= 0; i--) {
	 			if(state.carrito[i].divisa == 'D' && value.divisa == 'P'){
	 				//la divisa cambia
          state.carrito[i].divisa = 'P'
          //se actualizan los precios y los importes
          state.carrito[i].precio = accounting.unformat(state.carrito[i].precio) * value.tc
          state.carrito[i].precio = accounting.formatNumber(state.carrito[i].precio,2)

          state.carrito[i].preciopub = accounting.unformat(state.carrito[i].preciopub) * value.tc
          state.carrito[i].preciopub = accounting.formatNumber(state.carrito[i].preciopub, 2)

          state.carrito[i].importe = accounting.unformat(state.carrito[i].importe) * value.tc
          state.carrito[i].importe = accounting.formatNumber(state.carrito[i].importe, 2)
          
        }else if(state.carrito[i].divisa == 'P' && value.divisa == 'D'){
        	// La divisa se pasa a dolares
          state.carrito[i].divisa = 'D'
          // se actualizan los preciopubs y los importes
          state.carrito[i].precio = accounting.unformat(state.carrito[i].precio) / value.tc
          state.carrito[i].precio = accounting.formatNumber(state.carrito[i].precio,2)

          state.carrito[i].preciopub = accounting.unformat(state.carrito[i].preciopub) / value.tc
          state.carrito[i].preciopub = accounting.formatNumber(state.carrito[i].preciopub,2)

          state.carrito[i].importe = accounting.unformat(state.carrito[i].importe) / value.tc
          state.carrito[i].importe = accounting.formatNumber(state.carrito[i].importe,2)
        
        }else{
        	state.carrito[i].precio = accounting.unformat(state.carrito[i].precio) 
          state.carrito[i].precio = accounting.formatNumber(state.carrito[i].precio,2)

          state.carrito[i].preciopub = accounting.unformat(state.carrito[i].preciopub) 
          state.carrito[i].preciopub = accounting.formatNumber(state.carrito[i].preciopub,2)

          state.carrito[i].importe = accounting.unformat(state.carrito[i].importe) 
          state.carrito[i].importe = accounting.formatNumber(state.carrito[i].importe,2)
        }
			}
		},

	},

	actions:{
		carritoadd({commit}, articulo){
			// if(articulo.)
			var array = {
				codigo:  articulo.codigo,
				nomart:  articulo.nomart,
				precio1: articulo.precio1,
				pjedesc: articulo.pjedesc,
				descrip: articulo.descrip,
				foto:    articulo.fotos.length < 1 ? '' : articulo.fotos[0].image_name,
				cantidad: 1
			}
			commit('ADDCARRITO',array)
		},

		actualizaCarrito({commit}, divisa){
			return new Promise((resolve, reject) => {
				// sirve para cambiar la divisa del carrito
				commit('ACTUALIZARCARRITO', divisa)
				resolve(true)
			})
		},

		limpiarCarrito({commit}, value){
			return new Promise((resolve, reject) => {
				// Se vacia el carrito
				commit('LIMPIAR', value)
				resolve(true)
			})
		},


		traerTC({commit}, tc){
			return new Promise((resolve, reject) => {
				Vue.http.get('api/v1/tctienda').then(response=>{
					commit('TC',response.body.Tctienda)
					resolve(true)
				}).catch(error=>{
					console.log(error)
				})
			})
		}

  },

	getters:{
		getCarrito(state){
			return state.carrito
		},

		getTC(state){
			return state.tc
		}
	}
}